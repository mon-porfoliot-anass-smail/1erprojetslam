# Projet Slam 1

Mon projet slam de première année, était un site en html, css, et Javascript.
L'objectif de mon site était que les utilisateurs devait répondre à des questions via des inputs-radio et que selon les réponses choisis, un lien
contenant une redirection vers une page avec un bref descriptif d'un personnage était montré pour conseiller les visiteurs.
Pour cela, des liens était caché avec un style display none en dessous du formulaire.

```plantuml
:visiteur:
package Site-ProjetSsbu {
	visiteur-->(Formulaire-site)
	(Formulaire-site)..>(Personnage-Conseillé)
}
```

Voici le code des liens de redirection caché ainsi qu'un  aperçu du code du formulaire sur le site ainsi que son rendu sur un navigateur.
Code des liens de redirection caché:

![image du code des liens de redirection](LinkPagesProjetSlam1Display.png)

Formulaire:

![image du formulmaire du site](FormSsbu.png)

Code du formulaire

![image du code correspondant au form](LinkPagesProjetSlam1Form.png)


une fois les inputs cliqué, ma fonction checkMe en Javascript vérifiait, qu'elles sont les inputs qui ont était cliqué avant de montrer les liens invisible correspondant.
voici le code de ma fonction checkMe.

```html
<script type="text/javascript">
    	function checkMe(){
    		var ab = document.getElementById("Oui");
    		var bc = document.getElementById("Non");
    		var de = document.getElementById("Maniable");
    		var fg = document.getElementById("Technique");
    		var hi = document.getElementById("Court distance");
    		var jk = document.getElementById("Moyenne distance");
    		var lm = document.getElementById("Longue distance");
    		var text = document.getElementById("Min");
    		if (bc.checked==true && fg.checked==true && lm.checked==true){
    			text.style.display="block"
    		}else{
    			text.style.display="none"
    		}
    		var text = document.getElementById("Stev");
    		if (bc.checked==true && fg.checked==true && hi.checked==true){
    			text.style.display="block"
    		}else{
    			text.style.display="none"
    		}
    		var text = document.getElementById("Sephi");
    		if (bc.checked==true && de.checked==true && lm.checked==true){
    			text.style.display="block"
    		}else{
    			text.style.display="none"
    		}
    		var text = document.getElementById("PyMy");
    		if (bc.checked==true && de.checked==true && jk.checked==true){
    			text.style.display="block"
    		}else{
    			text.style.display="none"
    		}
    		var text = document.getElementById("Kazu");
    		if (ab.checked==true && fg.checked==true && hi.checked==true){
    			text.style.display="block"
    		}else{
    			text.style.display="none"
    		}
    		var text = document.getElementById("So");
    		if (bc.checked==true && de.checked==true && jk.checked==true){
    			text.style.display="block"
    		}else{
    			text.style.display="none"
    		}
    	}
    </script>
```

Une fois que le code ayant vérifié une des situations nous avons les liens qui ce montre selon les conditions vérifié.
Voici une des nombreuse pages descriptif des personnages:

![image du correspondant à la page de Sora](SoraPage.png)

les pages ce ressemble tous plus ou moins, certaines conditions amène à l'affichage de plusieurs liens.